package teamnorth.dashtillpuff;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;

/**
 * Created by Hunter on 5/21/2015.
 */
public class Ship implements TimeConscious {




    private int placement;
    private       int X;
    private       int Y;
    private       Bitmap bitmap;
    private       SurfaceView sv;
    public  static final int   RADIUS      = 100;


    public Ship( SurfaceView sv ) {
        this.sv = sv;

        BitmapFactory.Options options = new BitmapFactory.Options();
        this.bitmap = BitmapFactory.decodeResource(sv.getResources(),
                R.drawable.ship, options);
        this.Y = sv.getWidth()/2;

        this.X = 2 * sv.getWidth()/3;
    }


    public void setFall() {
        placement = 10; // Falling
    }

    public void setThrust() {
        placement = -10; // Rising

    }
    public void tick(Canvas canvas) {

        Y  += placement;
        if ( Y > sv.getHeight() - RADIUS ) {
            Y  = sv.getHeight() - RADIUS;
            placement = 0;
        } else if ( Y < RADIUS ) {
            Y  = RADIUS;
            placement = 0;
        }
        draw(canvas);
    }


    private void draw( Canvas c ) {
        Rect dst = new Rect( X-RADIUS, Y-RADIUS,
                X+RADIUS, Y+RADIUS);

        c.drawBitmap(bitmap, null, dst, null);

    }



}
