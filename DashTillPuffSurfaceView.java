package teamnorth.dashtillpuff;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Hunter on 4/30/2015.
 */
public class DashTillPuffSurfaceView extends SurfaceView
        implements SurfaceHolder . Callback , TimeConscious {


    private DashTillPuffBackground    background;
    private DashTillPuffRenderThread  renderThread;

    private Ship                    ship;

    public DashTillPuffSurfaceView ( Context context ) {
        super ( context ) ;
        getHolder () . addCallback ( this ) ;
    }
    @Override
    public void surfaceCreated ( SurfaceHolder holder ) {
        DashTillPuffRenderThread  renderThread = new DashTillPuffRenderThread ( this ) ;
        renderThread . start () ;

// Create the sliding background , cosmic factory , trajectory
// and the space ship
        Trajectory t= new Trajectory();

        CosmicFactory cosmicf = new CosmicFactory();

        BitmapFactory . Options options = new BitmapFactory . Options () ;
        Bitmap wallpaper = BitmapFactory. decodeResource(this.getResources(),
                R.drawable.wallpaper, options) ;
        Bitmap SpaceShip = BitmapFactory. decodeResource(this.getResources(),
                R.drawable.ship, options) ;




    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        renderThread = new DashTillPuffRenderThread(this);
        renderThread.start();
        background = new DashTillPuffBackground(this);

        ship     = new Ship(this);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public boolean onTouchEvent ( MotionEvent e ) {
        switch ( e . getAction () ) {
            case MotionEvent . ACTION_DOWN : // Thrust the space ship up .
                ship.setThrust();
                break ;
            case MotionEvent. ACTION_UP :
             // Let space ship fall freely .
                ship.setFall();
                break ;
        }
        return true ;
    }
    @Override
    public void onDraw ( Canvas c ) {
        super . onDraw ( c ) ;
// Draw everything ( restricted to the displayed rectangle ) .
    }
    @Override
    public void tick ( Canvas c ) {
// Tick background , space ship , cosmic factory , and trajectory .
// Draw everything ( restricted to the displayed rectangle ) .
        c.clipRect(0, 0, getWidth(), getHeight());
        background.fillBackground(c);
        background.tick(c);

        ship.tick(c);
    }
}
