package teamnorth.dashtillpuff;

/**
 * Created by Hunter on 4/30/2015.
 */
import android . graphics . Canvas ;
public interface TimeConscious {
    public void tick ( Canvas canvas ) ;
}
