package teamnorth.dashtillpuff;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceView;

/**
 * Created by Hunter on 5/27/2015.
 */
public class DashTillPuffBackground implements TimeConscious {


    protected final SurfaceView sv;
    protected final Bitmap bitmap;

    private int offsetX;
    public static final int OFFSET_X_INC  = 20;

    public DashTillPuffBackground( SurfaceView sv ) {
        this.sv = sv;


        BitmapFactory.Options options = new BitmapFactory.Options();

        bitmap = BitmapFactory.decodeResource(sv.getResources(),
                R.drawable.wallpaper, options);

        offsetX  = 0;



    }




    private void draw(Canvas c) {
        Rect src  = new Rect( 0, 0, bitmap.getWidth(), bitmap.getHeight());
        Rect dst1 = new Rect(- sv.getWidth()+offsetX, 0,                 offsetX,   sv.getHeight());
        Rect dst2 = new Rect(                offsetX, 0,   sv.getWidth()+offsetX,   sv.getHeight());
        c.clipRect( 0, 0,   sv.getWidth(), sv.getHeight() );
        c.drawBitmap(bitmap, null, dst1, null);
        c.drawBitmap(bitmap, null, dst2, null);
    }
    public void fillBackground(Canvas c) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        c.drawPaint(paint);

    }
    @Override
    public void tick(Canvas canvas) {
        int w = sv.getWidth();
        if ( true ) {
            offsetX += OFFSET_X_INC;
            if ( offsetX > w ) {
                offsetX -= w;
            }
        } else {
            offsetX -= OFFSET_X_INC;
            if ( offsetX < 0 ) {
                offsetX += w;
            }
        }
        draw(canvas);
    }



}
